package comp2931.cwk1;

import org.junit.Test;
import org.junit.Before;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class DateTest {

  private Date christmas;
  private Date febeleventh;

  @Before
  public void setUp(){
    christmas = new Date(2017, 12, 25);
    febeleventh = new Date(2017, 02, 11);
  }

  @Test
  public void dateToString() {
    assertThat(christmas.toString(), is("2017-12-25"));
    assertThat(febeleventh.toString(), is("2017-02-11"));

  }
  @Test
  public void equality() {
    assertTrue(christmas.equals(christmas));
    assertTrue(christmas.equals(new Date(2017, 12, 25)));
    assertFalse(christmas.equals(new Date(2013, 02, 10)));
    assertFalse(christmas.equals(new Date(2012, 11, 04)));
    assertFalse(christmas.equals(new Date(2012, 04, 31)));
    assertFalse(christmas.equals("2017-12-25"));
  }
  @Test(expected=IllegalArgumentException.class)
  public void daysTooLow() {
    new Date(0, 0, -1);
  }
  @Test(expected=IllegalArgumentException.class)
  public void daysTooHigh() {
    new Date(0, 0, 32);
  }
  @Test(expected=IllegalArgumentException.class)
  public void monthsTooHigh() {
    new Date(0, 13, 0);
  }
  @Test(expected=IllegalArgumentException.class)
  public void monthsTooLow() {
    new Date(0, -2, 0);
  }
  @Test(expected=IllegalArgumentException.class)
  public void yearsTooLow() {
    new Date(-3, 0, 0);
  }


}
