// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;


/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;



  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   *
   *@throws IllegalArgumentException
   */
  public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;
    // If the year is less than 0 it will throw an IllegalArgumentException
    if (year < 0){
      throw new IllegalArgumentException("Invalid Year.");
    }
    // if the month is less than 0 or more than 12 then it will throw an exception
    else if (month <= 0 || month > 12){
      throw new IllegalArgumentException("Invalid Month.");
    }
    // if the day is less than 0 or more than 31 then it will throw an exception
    else if (day <=0 || day > 31){
      throw new IllegalArgumentException("Invalid Day.");
    }
  }


  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
  *Tests to see if this date is equal to another
  *
  *@return true if the date object is equal to the other, false otherwise
  */
  public boolean equals(Object other) {
    if (other == this) {
      // checks to see if other is the same object as this one
      return true;
    }
    else if (! (other instanceof Date)) {
      // other is not a date object
      return false;
    }
    else {
      // compares both the fields
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
          && getMonth() == otherDate.getMonth()
          && getDay() == otherDate.getDay();
    }
  }
}
